/**
 * Created on: Jun 25, 2017
 * @author: hnguyen
 */
package com.cpa.sso.client;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.security.Principal;
import java.time.LocalDateTime;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import org.apache.catalina.filters.RequestDumperFilter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.Authentication;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configurers.ResourceServerSecurityConfigurer;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.authentication.OAuth2AuthenticationDetails;
import org.springframework.security.oauth2.provider.token.RemoteTokenServices;
import org.springframework.security.oauth2.provider.token.store.JwtAccessTokenConverter;
import org.springframework.util.StreamUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author Hieu Nguyen
 *
 */
@SpringBootApplication
@RestController
@EnableResourceServer
// @EnableOAuth2Sso
public class ResourceServerApplication extends ResourceServerConfigurerAdapter {

  final List<Message> messages = Collections.synchronizedList(new LinkedList<>());

  @Value("${oauth2.resource.id}")
  private String resourceId;

  @GetMapping("/api/messages")
  public List<Message> getMessages(Principal principal) {
    return messages;
  }

  @PostMapping("/api/messages")
  public Message postMessage(Principal principal, @RequestBody Message message) {
    message.username = principal.getName();
    message.createdAt = LocalDateTime.now();
    messages.add(0, message);
    return message;
  }

  @GetMapping("/api/me")
  public Authentication getMe(Authentication authentication) {
    OAuth2Authentication oAuth2Authentication = (OAuth2Authentication) authentication;
    final OAuth2AuthenticationDetails details = (OAuth2AuthenticationDetails) oAuth2Authentication.getDetails();
    
    return authentication;
  }

  public static class Message {
    public String text;

    public String username;

    public LocalDateTime createdAt;
  }

  public static void main(String[] args) {
    SpringApplication.run(ResourceServerApplication.class, args);
  }

  @Override
  public void configure(HttpSecurity http) throws Exception {
    http.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS).and().authorizeRequests()
        .antMatchers(HttpMethod.GET, "/api/**").access("#oauth2.hasScope('read')")
        .antMatchers(HttpMethod.POST, "/api/**").access("#oauth2.hasScope('write')");
  }

  /* (non-Javadoc)
   * @see org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter#configure(org.springframework.security.oauth2.config.annotation.web.configurers.ResourceServerSecurityConfigurer)
   */
  @Override
  public void configure(ResourceServerSecurityConfigurer resources) throws Exception {
    resources.resourceId(resourceId);
  }

  @Bean
  public JwtAccessTokenConverter accessTokenConverter() {
    JwtAccessTokenConverter converter = new JwtAccessTokenConverter();
    Resource resource = new ClassPathResource("cpa-jwt-pub.jks");
    String pubKey = null;
    try (InputStream io = resource.getInputStream()) {
      pubKey = StreamUtils.copyToString(io, StandardCharsets.ISO_8859_1);
    }
    catch (final IOException e) {
      throw new RuntimeException(e);
    }
    converter.setVerifierKey(pubKey);
    return converter;
  }

  /*  @Bean
  public AccessTokenConverter accessTokenConverter() {
    return new DefaultAccessTokenConverter();
  }*/

  /*  private TokenExtractor tokenExtractor = new BearerTokenExtractor();
  
  @Override
  public void configure(HttpSecurity http) throws Exception {
    http.addFilterAfter(new OncePerRequestFilter() {
      @Override
      protected void doFilterInternal(HttpServletRequest request,
          HttpServletResponse response, FilterChain filterChain)
          throws ServletException, IOException {
        // We don't want to allow access to a resource with no token so clear
        // the security context in case it is actually an OAuth2Authentication
        if (tokenExtractor.extract(request) == null) {
          SecurityContextHolder.clearContext();
        }
        filterChain.doFilter(request, response);
      }
    }, AbstractPreAuthenticatedProcessingFilter.class);
    http.csrf().disable();
    http.authorizeRequests().anyRequest().authenticated();
  }*/

  @Bean
  public RemoteTokenServices remoteTokenServices(
      final @Value("${oauth2.resource.tokenInfoUri}") String checkTokenUri,
      final @Value("${oauth2.client.clientId}") String clientId,
      final @Value("${oauth2.client.clientSecret}") String clientSecret) {
    final RemoteTokenServices remoteTokenServices = new RemoteTokenServices();
    remoteTokenServices.setCheckTokenEndpointUrl(checkTokenUri);
    remoteTokenServices.setClientId(clientId);
    remoteTokenServices.setClientSecret(clientSecret);
    remoteTokenServices.setAccessTokenConverter(accessTokenConverter());
    // remoteTokenServices.setAccessTokenConverter(accessTokenConverter());
    return remoteTokenServices;
  }

  @Bean
  public RequestDumperFilter requestDumperFilter() {
    return new RequestDumperFilter();
  }
}
